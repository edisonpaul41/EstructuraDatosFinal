/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectofinal;

/**
 *
 * @author paulmosquera
 */
public class MainWindow {

    public static void main(String[] args) {

        Grafo grafo = new Grafo();
        Nodo quito = new Nodo("quito");
        Nodo guayaquil = new Nodo("guayaquil");
        Nodo cuenca = new Nodo("cuenca");
        Nodo loja = new Nodo("loja");
        Nodo esmeraldas = new Nodo("esmeraldas");

        //quito.addLimite(new Limite(quito, guayaquil, 100));
        quito.addLimite(new Limite(quito, cuenca, 50));
        quito.addLimite(new Limite(quito, loja, 200));
        //quito.addLimite(new Limite(quito, esmeraldas, 80));

        cuenca.addLimite(new Limite(cuenca, quito, 50));
        cuenca.addLimite(new Limite(cuenca, guayaquil, 100));
        //cuenca.addLimite(new Limite(cuenca, loja, 70));
        cuenca.addLimite(new Limite(cuenca, esmeraldas, 150));

        guayaquil.addLimite(new Limite(guayaquil, quito, 100));
        guayaquil.addLimite(new Limite(guayaquil, cuenca, 100));
        //guayaquil.addLimite(new Limite(guayaquil, loja, 200));
        //guayaquil.addLimite(new Limite(guayaquil, esmeraldas, 50));

        //loja.addLimite(new Limite(loja, quito, 200));
        loja.addLimite(new Limite(loja, guayaquil, 200));
        //loja.addLimite(new Limite(loja, cuenca, 70));
        //loja.addLimite(new Limite(loja, esmeraldas, 250));

        esmeraldas.addLimite(new Limite(esmeraldas, quito, 80));
        //esmeraldas.addLimite(new Limite(esmeraldas, guayaquil, 50));
        //esmeraldas.addLimite(new Limite(esmeraldas, cuenca, 150));
        esmeraldas.addLimite(new Limite(esmeraldas, loja, 250));

        grafo.addNodo(quito);
        grafo.addNodo(guayaquil);
        grafo.addNodo(cuenca);
        grafo.addNodo(loja);
        //grafo.addNodo(esmeraldas);

        //grafo.buscarCamino("guayaquil");
        //System.out.println(grafo.getNodos().get(4));
        WUsuario panel = new WUsuario(grafo);
        panel.openWindow(grafo);
    }

}
