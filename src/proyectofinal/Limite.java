/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectofinal;

/**
 *
 * @author paulmosquera
 */
public class Limite {

    private Nodo origin;
    private Nodo destino;
    private double distancia;

    public Limite(Nodo origin, Nodo destino, double distancia) {
        this.origin = origin;
        this.destino = destino;
        this.distancia = distancia;
    }

    public Nodo getOrigin() {
        return origin;
    }

    public void setOrigin(Nodo origin) {
        this.origin = origin;
    }

    public Nodo getdestino() {
        return destino;
    }

    public void setdestino(Nodo destino) {
        this.destino = destino;
    }

    public double getdistancia() {
        return distancia;
    }

    public void setdistancia(double distancia) {
        this.distancia = distancia;
    }

    @Override
    public String toString() {
        return "\n Limite [origen=" + origin.getciudad()+ ", destino=" + destino.getciudad()+ ", distancia="
                + distancia + "]";
    }

}
