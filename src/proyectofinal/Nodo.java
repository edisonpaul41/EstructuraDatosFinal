/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectofinal;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Nodo {

    private String ciudad;
    private List<Limite> limites;

    public Nodo(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getciudad() {
        return ciudad;
    }

    public void setciudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public List<Limite> getlimites() {
        return limites;
    }

    public List<Limite> getLimitePorNombre(String ciudadIn) {
        System.out.println("Ciudad Buscada: " + ciudadIn);
        if (limites == null) {
            return null;
        }
        return limites.stream().filter(limite -> limite.getdestino().ciudad.contains(ciudadIn)).collect(Collectors.toList());
    }

    public void buscarCamino(int escalas, String destino, ArrayList<String> camino, Nodo nodoFinal, WUsuario context) {
        //System.out.println("Escalas " + escalas);
        if (camino == null && nodoFinal == null) {
            //System.out.println("Array Creado");
            camino = new ArrayList<String>();
            nodoFinal = this;
        }
        ArrayList<String> clone = (ArrayList<String>) camino.clone();

        //System.out.println("Ciudad pasada: " + nodoFinal.ciudad);
        if (escalas == 0 || destino == nodoFinal.ciudad) {
            clone.add(nodoFinal.getciudad());
            if (!clone.stream().filter(nodo -> nodo.contains(destino)).collect(Collectors.toList()).isEmpty()) {
                context.recibirCaminos(clone);
            }
        } else {
            escalas -= 1;
            clone.add(nodoFinal.getciudad());
            //System.out.println("Camino: " + camino);
            for (int a = 0; a < nodoFinal.getlimites().size(); a++) {
                buscarCamino(escalas, destino, clone, nodoFinal.getlimites().get(a).getdestino(), context);
                //System.out.println("Nodo pasado: " + nodoFinal.getlimites().get(a).getdestino());
            }
        }
    }

    public void addLimite(Limite limite) {
        if (limites == null) {
            limites = new ArrayList<>();
        }
        limites.add(limite);
    }
    
    public void eliminarLimite(String ciudadIn){
        System.out.println("enc " + getLimitePorNombre(ciudadIn).toString());
        if(getLimitePorNombre(ciudadIn) != null){
            try{
                          limites.remove(getLimitePorNombre(ciudadIn).get(0));
  
            }catch(Exception e){
                
            }
        }
        
    }

    @Override
    public String toString() {
        return "\n \tNode [ciudad=" + ciudad + ", limites=" + limites + "]";
    }

}
