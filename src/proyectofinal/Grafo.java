package proyectofinal;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author raidentrance
 *
 */
public class Grafo {
    
    private List<Nodo> nodos;
    
    public void addNodo(Nodo nodo) {
        if (nodos == null) {
            nodos = new ArrayList<>();
        }
        nodos.add(nodo);
    }
    
    public List<Nodo> getNodos() {
        return nodos;
    }
    
    public void buscarCamino(String origen, String destino, WUsuario context, int paradas) {
        getOrigen(origen).buscarCamino(paradas, destino, null, null, context);
    }
    
    @Override
    public String toString() {
        return "DATA: " + nodos;
    }
    
    private Nodo getOrigen(String ciudadIn) {
        try {
            return nodos.stream().filter(nodo -> nodo.getciudad().contains(ciudadIn)).collect(Collectors.toList()).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public void anadirRuta(String origen, int distancia, String destino) {
        Nodo origenNodo = getOrigen(origen);
        Nodo destinoNodo = getOrigen(destino);
        if (origenNodo == null) {
            origenNodo = new Nodo(origen);
            addNodo(origenNodo);
        }
        origenNodo.addLimite(new Limite(origenNodo, destinoNodo, distancia));
    }
    
    public void eliminarNodo(String ciudadIn){
        for(int a= 0 ; a< nodos.size(); a++){
            nodos.get(a).eliminarLimite(ciudadIn);
            System.out.println("aaa "+ ciudadIn);
        }
        nodos.remove(getOrigen(ciudadIn));
    }
    
}
